# README #

Bullet-Hell is a Bullet Hell Space Shooter.
The idea is to explore the genre and discover ways to create content quickly and efficiently.

![FINAL Title Screen.png](https://bitbucket.org/repo/ExRrXq/images/3810908664-FINAL%20Title%20Screen.png)

Play the game (win 64-bits, win x86, & WEBGL): [link](https://caiomga.itch.io/bullet-hell)

### GifScreens ###

[link](http://imgur.com/a/ksBm4)

### Inspirations ###

1. Ikaruga ([link](https://en.wikipedia.org/wiki/Ikaruga))
2. Touhou Project ([link](https://en.wikipedia.org/wiki/Touhou_Project))
3. Warning Forever ([link](https://en.wikipedia.org/wiki/Warning_Forever))

### Challenges ###
* Moving hundreds of objects on screen simultaneously in an efficient way
* Finish it until February 28th 2017
* Creating interesting and challenging boss designs
* Creating content quickly

![Iso Deca Hydra redone.PNG](https://bitbucket.org/repo/ExRrXq/images/3344113765-Iso%20Deca%20Hydra%20redone.PNG)

### Bosses ###
* Rookie One
* Second Best
* Rotor Motor Horror Motto
* Electric Hound Master
* Iso Deca Hydra

### Features Implemented ###

* Improve HUD / UI
* Different Ships with different features
* Achievement System
* Specials for each Energy
* Player specials + Special bar
* Player absorbing bullets from the same type as his
* Different Types of energy : Metal, Electric, Plasma and Soul
* Player skins for each energy
* Weapons for each energy
* Bullet Clear
* Modular bosses
* Bullet spread patterns
* Dual Analog stick inputs
* Camera Shake


### Features Considered ###

* Power Ups
* Level Up System
* Story Mode
* Boss Generation
* Multiplayer
* Bonus Levels