﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aWeaponSpin : MonoBehaviour {

	Animator anim;
	bool showing_a = true;

	void Start(){
		anim = GetComponent<Animator>();
	}

	public void spin(){
		if(showing_a){
			anim.SetTrigger("B");
		} else {
			anim.SetTrigger("A");
		}
		showing_a = !showing_a;
	}
}
