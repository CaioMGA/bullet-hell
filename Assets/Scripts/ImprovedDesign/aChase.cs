﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aChase : MonoBehaviour {

	public float speed;
	public Transform target;

	void FixedUpdate () {
		transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
	}
}
