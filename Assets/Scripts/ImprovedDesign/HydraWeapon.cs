﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HydraWeapon : MonoBehaviour {

	public bool hittable;

	aB5 hydra;

	void Start(){
		hydra = FindObjectOfType<aB5>().transform.GetComponent<aB5>();
		hittable = false;
	}

	void OnTriggerEnter(Collider other){
		if(other.transform.CompareTag("HERO Bullet")){
			//other.GetComponentInParent<BulleScript>().recycle();
			if(hittable){
				hydra.head_damaged();
			}
		}
	}
}
