﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aLife : MonoBehaviour {
	public float total_life;
	public float current_life;
	public GameObject shield;

	bool invencible = true;
	bool showing_shield = true;

	void Start(){
		current_life = total_life;
	}

	void Update(){
		if(invencible){
			if(!showing_shield){
				shield.SetActive(true);
				showing_shield = true;
			}
		} else {
			if(showing_shield){
				shield.SetActive(false);
				showing_shield = false;
			}
		}

		if(current_life < 0){
			current_life = 0;
		}
	}

	public void set_invencibility(bool value){
		invencible = value;
	}

	void OnTriggerEnter(Collider other){
		if(other.transform.CompareTag("HERO Bullet")){
			other.GetComponentInParent<BulleScript>().recycle();
			if(!invencible){
				current_life -= 0.1f;
			}
			if(current_life < 0){
				GetComponent<Collider>().enabled = false;
				GetComponent<aWeapon>().shooting.hold_fire();
				current_life = 0;
			}
		}
	}

	public void die(){
		shield.SetActive(false);
		showing_shield = false;
		GetComponent<Collider>().enabled = false;
		//show smoke and sparkles to convey malfunction
	}
}
