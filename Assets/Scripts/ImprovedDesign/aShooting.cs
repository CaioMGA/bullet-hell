﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aShooting : MonoBehaviour {
	public float bullet_speed = 150f;
	public float bullet_interval = 1f;
	public BulletPool bullet_pool;

	protected float bullet_deltaTime = 0;
	protected Transform bullet_deploy;

	public bool firing = false;

	void Start(){
		hold_fire();
		bullet_deploy = transform.GetChild(0).GetComponent<Transform>();
	}

	void Update(){
		if(firing){
			if(bullet_deltaTime >= bullet_interval){
				shoot();
				bullet_deltaTime = 0;
			} else {
				bullet_deltaTime += Time.deltaTime;
			}
		}
	}

	public void hold_fire(){
		firing = false;
	}

	public void fire(){
		firing = true;
		bullet_deltaTime = 0;
	}

	void shoot(){
		//get a shootable bullet
		GameObject newBullet = bullet_pool.getBullet();
		if(newBullet != null){
			//put bullet on screen and make it move
			newBullet.transform.rotation = bullet_deploy.transform.rotation;
			newBullet.transform.position = bullet_deploy.transform.position;
			newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * -bullet_speed);
		} else {
			Debug.Log("No bullets left to shoot in " + bullet_pool.name);
			return;
		}
	}
}
