﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aElectricBeamShooter : aShooting {
	/* attack cycle
	 * idle -> telegraph -> attack
	 */
	//bullet_interval == idle time
	public float telegraph_time = 2f;
	public float attack_time = 5f;
	public Texture telegraph_frame;

	Animator anim;
	Collider collider;
	Material mat;
	TextureAnimation attack_animation;
	MeshRenderer rend;

	bool is_idle = false;

	void Start(){
		anim = GetComponent<Animator>();
		collider = GetComponent<Collider>();
		attack_animation = GetComponent<TextureAnimation>();
		rend = GetComponent<MeshRenderer>();
		mat = GetComponent<Renderer>().material;
		telegraph_time += bullet_interval;
		attack_time += telegraph_time;
		firing = false;
	}

	void Update(){
		if(firing){
			if(bullet_deltaTime >= attack_time){ // repeat
				bullet_deltaTime = 0;
				idle();
			} else if(bullet_deltaTime >= telegraph_time){ // show attack
				bullet_deltaTime += Time.deltaTime;
				attack();
			} else if(bullet_deltaTime >= bullet_interval){ // show telegraph
				bullet_deltaTime += Time.deltaTime;
				telegraph();
			} else { // keep in idle
				bullet_deltaTime += Time.deltaTime;
			}
		} else {
			if(!is_idle){
				idle();
			}
		}
	}

	void idle(){
		collider.enabled = false;
		rend.enabled = false;
		attack_animation.enabled = false;
		is_idle = true;
	}

	void telegraph(){
		collider.enabled = false; //just in case something goes wrong
		attack_animation.enabled = false;
		rend.enabled = true;
		mat.SetTexture("_MainTex", telegraph_frame);
		is_idle = false;
	}

	void attack(){
		collider.enabled = true;
		rend.enabled = true;
		attack_animation.enabled = true;
		is_idle = false;
	}

	public void shoot(){ // overrides parent's function
		telegraph();
	}

	public void hold_fire(){
		firing = false;
		idle();
	}
}
