﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aArcShooter : aShooting {

	public int bullet_count = 16;
	Vector3 dir;

	void Update(){
		if(firing){
			if(bullet_deltaTime >= bullet_interval){
				shoot();
				bullet_deltaTime = 0;
			} else {
				bullet_deltaTime += Time.deltaTime;
			}
		}
	}

	void shoot(){
		float bullet_angle = 360f / ((float)bullet_count);
		GameObject newBullet;
		for (int i = 0; i < bullet_count; i++){
			newBullet = bullet_pool.getBullet();
			if(newBullet == null){
				return;
			} else {
				newBullet.transform.position = transform.position;
				newBullet.transform.rotation = Quaternion.Euler(dir);
				newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed);
				dir = new Vector3 (0, dir.y + bullet_angle, 0);
			}
		}
	}
}
