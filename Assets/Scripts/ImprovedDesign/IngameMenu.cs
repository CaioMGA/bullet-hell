﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IngameMenu : MonoBehaviour {
	public GameObject pause_menu;
	public GameObject restart_prompt;
	public GameObject quit_prompt;

	void Start () {
		
	}
	

	void Update () {
		if(Input.GetButtonDown("Pause")){
			if(Time.timeScale == 0){
				show_menu(false);
			} else {
				show_menu(true);
			}
		}
	}

	void show_menu(bool value){
		pause_menu.SetActive(value);
		quit_prompt.SetActive(false);
		restart_prompt.SetActive(false);
		if(!value){
			Time.timeScale = 1;
		} else {
			Time.timeScale = 0;
		}
	}

	public void resume(){
		show_menu(false);
	}

	public void prompt_restart(){
		restart_prompt.SetActive(true);
	}

	public void prompt_quit(){
		quit_prompt.SetActive(true);
	}

	public void restart(){
		Time.timeScale = 1;
		SceneManager.LoadScene(1);
	}

	public void quit(){
		Time.timeScale = 1;
		SceneManager.LoadScene(0);
	}

	public void back_to_pause(){
		show_menu(true);

	}
}
