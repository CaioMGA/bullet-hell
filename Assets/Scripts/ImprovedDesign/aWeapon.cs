﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aWeapon : MonoBehaviour {
	public bool active = false;
	public aLife life;
	public aShooting shooting;
	public MaterialType material; //what for?

	void Start(){
		active = true;
		stop();
	}

	public void explode(){
		shooting.hold_fire();
		life.die();
		active = false;
		//paint it gray
	}

	public void stop(){
		if(life.current_life > 0){
			life.set_invencibility(true);
			shooting.hold_fire();
		}
	}

	public void resume(){
		if(life.current_life > 0){
			life.set_invencibility(false);
			shooting.fire();
		}
	}
}
