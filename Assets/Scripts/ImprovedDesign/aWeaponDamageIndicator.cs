﻿using UnityEngine;
using System.Collections;

public class aWeaponDamageIndicator : MonoBehaviour {
	public Color full_life;
	public Color no_life;

	public aLife ls;
	float last_current_life = 0;
	MeshRenderer rend;

	void Start(){
		ls = transform.GetComponentInParent<aLife>();
		/*
		if(ls == null){
			ls = GetComponent<aLife>();
		}
		*/
		rend = GetComponent<MeshRenderer>();
	}

	void Update(){
		if(ls.current_life == last_current_life){
			//do nothing
		} else {
			float r, g, b, dmg_percent;
			dmg_percent = 1 - (ls.current_life / ls.total_life);
			r = full_life.r - ((full_life.r - no_life.r) * dmg_percent);
			g = full_life.g - ((full_life.g - no_life.g) * dmg_percent);
			b = full_life.b - ((full_life.b - no_life.b) * dmg_percent);
			rend.material.SetColor("_Color", new Color(r, g, b));
			last_current_life = ls.current_life;
		}
	}
}
