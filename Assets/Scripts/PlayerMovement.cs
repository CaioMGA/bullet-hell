﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public float speed = 50;
	public int input_type = 0;
	public int ship_type = 0;

	Rigidbody rb;
	Vector3 inputs;
	Vector3 raw_inputs;
	float rotation;

	bool locked_rotation = false;

	void Start(){
		rb = GetComponent<Rigidbody>();
		input_type = PlayerPrefs.GetInt("Input Type");
		ship_type = PlayerPrefs.GetInt("Ship Type");
	}

	void get_inputs(){
		if(input_type == 0){
			float mov_x = 0;
			float mov_y = 0;
			if(Input.GetKey(KeyCode.A)){
				mov_x -= 1;
			}
			if(Input.GetKey(KeyCode.D)){
				mov_x += 1;
			}
			if(Input.GetKey(KeyCode.W)){
				mov_y += 1;
			}
			if(Input.GetKey(KeyCode.S)){
				mov_y -= 1;
			}

			inputs = new Vector3(mov_x, mov_y, 0f);
			raw_inputs = new Vector2(mov_x, mov_y);
		} else if(input_type == 1){
			inputs = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0f);
			raw_inputs = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		}

		if(ship_type == 1){
			if(Input.GetButtonDown("Lock Rotation")){
				locked_rotation = !locked_rotation;
			}
		}

	}

	void calculate_rotation(){
		if(!locked_rotation){
			if(inputs.sqrMagnitude < 0.01f){
				return;
			}
			rotation = Mathf.Atan2(inputs.x, inputs.y) * Mathf.Rad2Deg;
		}
	}

	void rotate_me(){
		if(!locked_rotation){
			transform.rotation = Quaternion.Euler(0, rotation, 0);
		}
	}

	void move(){
		if(ship_type == 1 && locked_rotation){
			if(raw_inputs.x != 0f || raw_inputs.y != 0f){
				float ratio = inputs.magnitude;
				Vector3 dir = new Vector3(inputs.x, 0, inputs.y);
				rb.AddForce(dir * speed * ratio);
				if(rb.velocity.magnitude * ratio > speed){
					rb.velocity = rb.velocity.normalized * speed * ratio;
				}
			}
		} else if(ship_type != 2 ){
			if(raw_inputs.x != 0f || raw_inputs.y != 0f){
				float ratio = inputs.magnitude;
				rb.AddRelativeForce(Vector3.forward * speed * ratio);
				if(rb.velocity.magnitude * ratio > speed){
					rb.velocity = rb.velocity.normalized * speed * ratio;
				}
			}
		} else {
			if(raw_inputs.x != 0f || raw_inputs.y != 0f){
				float ratio = inputs.magnitude;
				Vector3 dir = new Vector3(inputs.x, 0, inputs.y);
				rb.AddRelativeForce(dir * speed * ratio);
				if(rb.velocity.magnitude * ratio > speed){
					rb.velocity = rb.velocity.normalized * speed * ratio;
				}
			}
		}

	}

	void FixedUpdate(){
		get_inputs();
		if(ship_type != 2 || (ship_type == 1 && !locked_rotation)){
			calculate_rotation();
			rotate_me();
		} 
		move();
	}

	public void update_input_type(int _input_type){
		input_type = _input_type;
	}
}