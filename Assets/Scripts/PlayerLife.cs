﻿using UnityEngine;
using System.Collections;

public class PlayerLife : MonoBehaviour {
	public int lives = 3;
	public bool damage_registered = false;
	public bool invencible = true;
	public AudioClip death_clip;

	float invencibility_time = 1f; //starts 1 second invencible
	float invencibility_deltatime = 0f;

	GameLogic gl;
	AudioSource audioSource;
	PlayerType pt;

	void Start(){
		gl = GameObject.FindGameObjectWithTag("GameLogic").GetComponent<GameLogic>();
		audioSource = GetComponent<AudioSource>();
		pt = GetComponentInParent<PlayerType>();
	}

	void Update(){
		if(invencible){
			if(invencibility_deltatime >= invencibility_time){
				invencible = false;
				damage_registered = false;
			} else {
				invencibility_deltatime += Time.deltaTime;
			}
		}
	}

	void OnTriggerEnter(Collider other){
		if(!other.transform.CompareTag("HERO Bullet") && !other.transform.CompareTag("Player Range") && !other.transform.CompareTag("Out Of Bounds")){
			MaterialType matType = other.GetComponent<MaterialType>();
			if(matType != null){
				if(pt.type == matType.type){
				} else if(!invencible){
					register_damage();
				}
			}
		}
	}

	void register_damage(){
		damage_registered = true;
		lives-=1;
		gl.kill_all_bullets();
		audioSource.PlayOneShot(death_clip);
		set_invencible(10f);
	}

	void set_invencible(float time){
		invencibility_time = time;
		invencibility_deltatime = 0;
		invencible = true;

	}
}