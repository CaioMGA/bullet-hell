﻿using UnityEngine;
using System.Collections;

public class WeaponDamageIndicator : MonoBehaviour {
	public Color full_life;
	public Color no_life;

	public LifeScript ls;
	float last_current_life = 0;
	MeshRenderer rend;

	void Start(){
		ls = transform.GetComponentInParent<LifeScript>();
		rend = GetComponent<MeshRenderer>();
	}

	void Update(){
		if(ls.life_current == last_current_life){
			//do nothing
		} else {
			float r, g, b, dmg_percent;
			dmg_percent = 1 - (ls.life_current / ls.life_total);
			//float r, g, b;
			r = full_life.r - ((full_life.r - no_life.r) * dmg_percent);
			g = full_life.g - ((full_life.g - no_life.g) * dmg_percent);
			b = full_life.b - ((full_life.b - no_life.b) * dmg_percent);
			rend.material.SetColor("_Color", new Color(r, g, b));
			last_current_life = ls.life_current;
		}
	}
}
