﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerSpecial : MonoBehaviour {
	public float MAX = 100;
	public float fill = 0f;
	public Color [] special_bar_fill_color;
	public GameObject special_animation;
	public bool fighting_hydra = false;

	int current_type = -1;
	float absorb_deltatime = 0f;
	float absorb_interval = 0.12f;
	float ratio;
	public PlayerType pt;
	GameLogic gl;
	Slider special_bar;
	Text special_label;
	Image special_bar_fill;
	aB5Core hydra_core;

	// achievements 
	bool achievement_gauge_lvl_1 = false;
	bool achievement_gauge_lvl_MAX = false;
	bool achievement_SPECIAL_lvl_1 = false;
	bool achievement_SPECIAL_lvl_MAX = false;


	void Start(){
		init_achievements();
		special_bar = GameObject.FindGameObjectWithTag("Special Bar").GetComponent<Slider>();
		special_label = GameObject.FindGameObjectWithTag("Special Bar Label").GetComponent<Text>();
		pt = GetComponent<PlayerType>();
		special_bar_fill = GameObject.FindGameObjectWithTag("Special Bar Fill").GetComponent<Image>();
		gl = GameObject.FindGameObjectWithTag("GameLogic").GetComponent<GameLogic>();
		special_animation = GameObject.FindGameObjectWithTag("Special Model");
		special_animation.SetActive(false);

	}

	void Update(){
		if(current_type != pt.type){
			current_type = pt.type;
			special_bar_fill.color = special_bar_fill_color[current_type];
		}
		ratio = fill / MAX;
		if(ratio >= 0.2f && Input.GetButtonDown("SPECIAL Attack")){
			SPECIAL_ATTACK_KATIAU();
		}
	}

	public void absorb(){
		fill += 0.3f;
		update_HUD();
	}

	public void update_HUD(){
		if(fill > MAX){
			fill = MAX;
		}
		special_bar.value = ratio;

		if(ratio == 1f){
			special_label.text = "MAX";
			if(!achievement_gauge_lvl_MAX){
				achievement_gauge_lvl_MAX = true;
				gl.GetComponent<AchievementSystem>().register_achievement(9);
			}
		} else {
			int txt = Mathf.FloorToInt(ratio * 5f);
			if(txt == 1 && !achievement_gauge_lvl_1){
				achievement_gauge_lvl_1 = true;
				gl.GetComponent<AchievementSystem>().register_achievement(8);
			}
			special_label.text = txt.ToString();
		}
	}

	void OnTriggerStay(Collider other){
		if(!other.transform.CompareTag("HERO Bullet") && !other.transform.CompareTag("Player Range") && !other.transform.CompareTag("Out Of Bounds")){
			MaterialType matType = other.GetComponent<MaterialType>();
			if(matType != null){
				if(pt.type == matType.type){
					BulleScript bs = other.GetComponent<BulleScript>();
					if(bs != null){ //if it is a projectile
						absorb();
						bs.recycle();
					} else { // if it is electric damage
						if(absorb_deltatime >= absorb_interval){
							absorb();
							absorb_deltatime = 0;
						} else {
							absorb_deltatime += Time.deltaTime;
						}
					}
				}

			}
		}
	}

	void SPECIAL_ATTACK_KATIAU(){
		//achievement
		if(ratio == 1 && !achievement_SPECIAL_lvl_MAX){
			gl.GetComponent<AchievementSystem>().register_achievement(12);
			achievement_SPECIAL_lvl_MAX = true;
			gl.GetComponent<AchievementSystem>().register_achievement(11);
			achievement_SPECIAL_lvl_1 = true;
		} else if(!achievement_SPECIAL_lvl_1){
			gl.GetComponent<AchievementSystem>().register_achievement(11);
			achievement_SPECIAL_lvl_1 = true;
		}

		//attack
		gl.kill_all_bullets();
		if(fighting_hydra){
			hydra_core.life.current_life -= (Mathf.FloorToInt(ratio * 5) * 2f) * 20;
			Debug.Log("KATIAU! Hydra");
		} else {
			gl.damage_all_enemies(Mathf.FloorToInt(ratio * 5) * 2f); 
			Debug.Log("KATIAU!");
		}
		special_animation.SetActive(false);
		special_animation.SetActive(true);
		ratio = 0;
		fill = 0;
		update_HUD();
	}

	public void hydra_boss(aB5Core c){
		fighting_hydra = true;
		hydra_core = c;
	}

	void init_achievements(){
		achievement_gauge_lvl_1 = (PlayerPrefs.GetInt("achievement_8") == 1) ? true : false;
		achievement_gauge_lvl_MAX = (PlayerPrefs.GetInt("achievement_9") == 1) ? true : false;
		achievement_SPECIAL_lvl_1 = (PlayerPrefs.GetInt("achievement_11") == 1) ? true : false;
		achievement_SPECIAL_lvl_MAX = (PlayerPrefs.GetInt("achievement_12") == 1) ? true : false;
	}
}
