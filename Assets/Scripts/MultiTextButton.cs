﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiTextButton : MonoBehaviour {

	public string [] labels;
	public string PlayerPref_name;
	Button btn;
	int label_index = 0;

	void Start(){
		btn = GetComponent<Button>();
		label_index = PlayerPrefs.GetInt(PlayerPref_name);
		change_text();
	}

	public void next_text(){
		label_index += 1;
		if(label_index >= labels.Length){
			label_index = 0;
		}
		update_playerpref();
		change_text();
	}

	public void prev_text(){
		label_index -= 1;
		if(label_index < 0){
			label_index = labels.Length - 1;
		}
		update_playerpref();
		change_text();
	}

	void change_text(){
		btn.GetComponentInChildren<Text>().text = labels[label_index];
	}

	void update_playerpref(){
		PlayerPrefs.SetInt(PlayerPref_name, label_index);
	}
}
