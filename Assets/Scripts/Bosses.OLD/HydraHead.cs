﻿using UnityEngine;
using System.Collections;

public class HydraHead : MonoBehaviour {
	public bool deploy_new_head = false;
	public float deploy_speed = 80f;
	public bool damageable = false;

	GameObject shield;
	int type;
	Transform target;

	public bool deploying = false;
	bool showing_shield = false;


	void Start(){
		shield = transform.GetChild(1).gameObject;
		type = GetComponent<MaterialType>().type;

	}

	void Update(){
		if(deploying){
			transform.position = Vector3.MoveTowards(transform.position, target.position, deploy_speed * Time.deltaTime);
			if (transform.position == target.position){
				deploying = false;
			}
		}

		if(damageable){
			if(showing_shield){
				shield.SetActive(false);
				showing_shield = false;
			}
		} else if(!showing_shield){
			shield.SetActive(true);
			showing_shield = true;
		}
	}

	void new_head(){
		deploy_new_head = true;
	}

	void OnTriggerEnter(Collider other){
		if(damageable){
			if(!deploy_new_head){
				if(other.CompareTag("HERO Bullet")){
					new_head();
					other.GetComponentInParent<BulleScript>().recycle();
				} else {

				}
			}
		}
	}

	public void Enter(Transform t){
		//move to t
		target = t;
		deploying = true;
	}
}
