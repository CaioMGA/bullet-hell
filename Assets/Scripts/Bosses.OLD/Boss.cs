﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Boss : MonoBehaviour{

	public float [] phases;
	public GameObject [] weapons;
	public LifeScript [] guns_lives;
	public float life_total;
	public float life_current;
	public int current_phase = 0;

	GameLogic gl;

	public virtual void Enter(){
		Debug.Log("If you see this, you did not create the boss script (ex.: B01.cs) correctly");
	}

	public int get_active_weapons_count(){
		int total = 0;
		foreach(LifeScript ls in guns_lives){
			if(ls.alive){
				total += 1;
			}
		}
		return total;
	}
}
