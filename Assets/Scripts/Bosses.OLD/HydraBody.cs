﻿using UnityEngine;
using System.Collections;

public class HydraBody : MonoBehaviour {
	public bool damageable = false;
	public HydraHead [] heads;
	public Transform [] head_place_holders;
	public bool deploying_head = false;

	HydraHead newest_head = null;
	int head_count;
	int heads_shown;
	float damage_timer;

	void Start(){
		//heads = GetComponentsInChildren<HydraHead>();
		head_place_holders = new Transform[10];
		heads = new HydraHead[10];
		for(int i = 0; i < 10; i++){
			head_place_holders[i] = transform.GetChild(i);
			heads[i] = transform.GetChild(10 + i).GetComponent<HydraHead>();
		}
		head_count = heads.Length;
		heads_shown = 2;
	}

	public void check_heads(){
		foreach(HydraHead h in heads){
			if(h.deploy_new_head && !deploying_head){
				if(heads_shown < head_count){
					heads[heads_shown].gameObject.SetActive(true);
					heads[heads_shown].Enter(head_place_holders[heads_shown]);
					heads_shown += 1;
					h.deploy_new_head = false;
					deploying_head = true;
					newest_head = h;
					set_heads_damageable(false);
					set_body_damageable(false);
					return;
				}
			}
		}
		if(newest_head != null){
			if(!newest_head.deploying){
				newest_head = null;
				deploying_head = false;
				//set_heads_damageable(true);
				//set_body_damageable(true);
			}
		}
	}

	public void set_heads_damageable(bool value){
		foreach(HydraHead h in heads){
			h.damageable = value;
			//damage_timer = time;
		}
	}

	public void set_body_damageable(bool value){
		damageable = value;
	}
}
