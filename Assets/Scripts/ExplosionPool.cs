﻿using UnityEngine;
using System.Collections;

public class ExplosionPool : MonoBehaviour {
	public int explosions_qtd = 200;
	public int explosion_index = 0;
	public GameObject explosion;

	void Start(){
		for(int i = 0; i < explosions_qtd; i++){
			GameObject new_explosion = Instantiate(explosion);
			new_explosion.transform.parent = transform;
		}
	}

	public GameObject get_explosion(){
		if(explosion_index < 200){
			GameObject this_explosion = transform.GetChild(explosion_index ).gameObject;
			this_explosion.SetActive(false);
			this_explosion.SetActive(true);
			explosion_index += 1;
			return this_explosion;
		}
		return null;
	}
}
