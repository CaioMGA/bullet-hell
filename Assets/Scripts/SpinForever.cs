﻿using UnityEngine;
using System.Collections;

public class SpinForever : MonoBehaviour {
	public Vector3 spin_axis_speed;
	public bool spin = true;

	Vector3 spin_axis_rotation;

	void Start(){
		spin_axis_rotation = new Vector3(
			transform.localRotation.eulerAngles.x,
			transform.localRotation.eulerAngles.y,
			transform.localRotation.eulerAngles.z
		);
			}

	void Update(){
		
		if(spin){
			spin_axis_rotation += spin_axis_speed * Time.deltaTime;
			transform.localRotation = Quaternion.Euler(spin_axis_rotation);
		}
	}
}
