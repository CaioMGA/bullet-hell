﻿using UnityEngine;
using System.Collections;

public class LifeScript : MonoBehaviour {
	public bool damageable = false;
	public bool hittable = true;
	public bool alive = true;
	public bool this_is_player = false;
	public float life_total = 100;
	public float life_current;
	public string bullet_tag = "";

	public GameObject shield;

	bool showing_shield = false;

	void Start () {
		init();
	}
	
	void Update () {
		if(life_current <= 0){
			alive = false;
			damageable = false;
			GetComponent<Collider>().enabled = false;
		}
		if(alive){
			if(!damageable){
				if(!showing_shield){
					show_shield();
				}
			} else {
				if(showing_shield){
					hide_shield();
				}
			}
		}
	}

	void init(){
		life_current = life_total;
		if(this_is_player){
			bullet_tag = "Bullet";
		} else {
			bullet_tag = "HERO Bullet";
		}

		if(shield.activeInHierarchy){
			showing_shield = true;
		} else {
			showing_shield = false;
		}
	}

	void OnTriggerEnter(Collider other){
		if(other.transform.CompareTag(bullet_tag)){
			if(hittable){
				other.GetComponentInParent<BulleScript>().recycle();
				if(damageable){
					life_current -= 0.1f; // HERO bullet DAMAGE
				}
			}
		}
	}

	void show_shield(){
		shield.SetActive(true);
		showing_shield = true;
	}

	void hide_shield(){
		shield.SetActive(false);
		showing_shield = false;
	}
}
