﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Achievement: System.Object {

	public string title;
	public string description;
	public Sprite icon;

	public Achievement(string _title, string _description, Sprite _icon){
		title = _title;
		description = _description;
		icon = _icon;
	}
}
