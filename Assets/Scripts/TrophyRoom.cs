﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrophyRoom : MonoBehaviour {
	public Sprite [] icons;
	public GameObject [] go;
	public Image [] badges;
	public string [] descriptions;
	public string default_description;
	public Text description_box;
	public Text death_count;

	int deaths;
	int [] achievements;

	void Start(){
		deaths = PlayerPrefs.GetInt("Deaths");
		death_count.text = "Deaths: " + deaths;
		achievements = new int[13];
		for(int i = 0; i< 13; i++){
			achievements[i] = PlayerPrefs.GetInt("achievement_" + i);
		}
		init_badges();
		show_achievements_earned();
	}

	public void reset_achievements(){
		for(int i = 0; i< 13; i++){
			PlayerPrefs.SetInt("achievement_" + i, 0);
		}
		PlayerPrefs.SetInt("Deaths", 0);
	}

	void show_achievements_earned(){
		for(int i = 0; i< 13; i++){
			if(achievements[i] == 1){
				badges[i].sprite = icons[i];
			}
		}
	}

	public void init_badges(){
		badges = new Image[13];
		for(int i = 0; i < 13; i++){
			badges[i] = go[i].GetComponent<Image>();
		}
	}

	public void show(int index){
		description_box.text = descriptions[index];
		Debug.Log("description changed");
	}

	public void show_default(){
		description_box.text = default_description;
	}

	public void refresh(){
		Start();
	}
}
