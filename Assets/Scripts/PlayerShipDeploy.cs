﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipDeploy : MonoBehaviour {

	int ship_type;

	public GameObject [] ships;
	public void init () {
		ship_type = PlayerPrefs.GetInt("Ship Type");
		GameObject ship = Instantiate(ships[ship_type]);
		GetComponent<GameLogic>().player = ship;
	}


}
