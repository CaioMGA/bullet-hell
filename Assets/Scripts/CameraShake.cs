﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {
	float intensity;
	float duration;
	public bool fadein;
	public bool fadeout;
	float shake_delay;

	float fadein_muliplicator = 0.1f;
	bool shaking = false;
	float shaking_elapsed_time = 0;
	Vector3 original_state;

	// Use this for initialization
	void Start () {
		original_state = GetComponent<Transform>().position;

	}
	
	// Update is called once per frame
	void Update () {
		if (shaking){
			if(shaking_elapsed_time >= shake_delay){
				if(shaking_elapsed_time  >= duration){
					shaking = false;
					transform.position = original_state;
				} else {
					shaking_elapsed_time += Time.deltaTime;
					transform.position = new Vector3(
						Random.Range(-intensity, intensity) * fadein_muliplicator + original_state.x,
						original_state.y,
						Random.Range(-intensity, intensity) * fadein_muliplicator + original_state.z
					);
					if(fadein_muliplicator < 1){
						fadein_muliplicator += 0.01f;
					}
					if(fadein_muliplicator > 1){
						fadein_muliplicator = 1;;
					}
				}
			} else {
				shaking_elapsed_time += Time.deltaTime;
			}
		} 
	}

	public void shake(float _intensity, float _duration, float _delay = 0, bool _fadein = false, bool _fadeout = false){
		shaking = true;
		intensity = _intensity;
		duration = _duration;
		shaking_elapsed_time = 0;
		shake_delay = _delay;
		fadein = _fadein;
		fadeout = _fadeout;
	}
}
