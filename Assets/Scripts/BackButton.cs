﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButton : MonoBehaviour {
	
	public void go_back(){
		Debug.Log(transform.parent.gameObject);
		transform.parent.gameObject.SetActive(false);
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			go_back();
		}
	}

}
