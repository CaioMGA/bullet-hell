﻿using UnityEngine;
using System.Collections;

//BulletPool is attached to a GameObject that holds bullets of a given type
//it keep tracks of what bullets were shot
//and recycle those that hit something or went out of the screen
//
//IMPORTANT
//bullet's gameObject must be INACTIVE

public class BulletPool : MonoBehaviour {
	public int qtd_bullets = 100;
	public GameObject bullet;

	public bool[] active_bullets;
	void Start () {
		active_bullets = new bool[qtd_bullets];
		for(int i = 0; i < qtd_bullets; i ++){
			GameObject new_bullet = Instantiate(bullet);
			new_bullet.transform.parent = transform;
			//verify line above
			new_bullet.GetComponent<BulleScript>().bullet_index = i;

			active_bullets[i] = false;//inactive
		}
	}
	

	public GameObject getBullet() {
		for(int i = 0; i < qtd_bullets; i++){
			if(active_bullets[i] == false){
				active_bullets[i] = true;
				transform.GetChild(i).gameObject.SetActive(true);
				Rigidbody rb = transform.GetChild(i).GetComponent<Rigidbody>();
				rb.velocity = Vector3.zero;
				rb.angularVelocity = Vector3.zero;
				return transform.GetChild(i).gameObject;
			}
		}
		return null; //out of bullets
	}

	public void deactivate_bullet(int bullet_index){
		transform.GetChild(bullet_index).gameObject.SetActive(false);
		active_bullets[bullet_index] = false;
	}
}
