﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour {
	// work in progress
	public Text title;
	public Text by_caio;
	public Text press_any_key;
	public GameObject main_menu;
	public GameObject title_menu;
	public GameObject new_game_panel;
	public GameObject settings_panel;
	public GameObject credits_panel;
	public GameObject select_ship_panel;
	public GameObject checkout;
	public GameObject trophy_room;
	public GameObject settings_audio;
	public GameObject settings_arcade;
	public GameObject how_to_play;
	public GameObject reset_achievements;
	public GameObject prompt_quit;


	float deltatime = 0;
	float show_title_timestamp = 2f;
	float show_by_caio_timestamp = 4f;
	float show_press_any_key_timestamp = 8f;
	float wait_for_key_press = 8.5f;

	bool intro_completed = false;

	void Start(){
		//trophy_room.SetActive(false);
	}

	void Update () {
		if(!intro_completed){
			if(deltatime >= wait_for_key_press){
				//do nothing
				if(Input.anyKeyDown){
					main_menu.SetActive(true);
					title_menu.SetActive(false);
					intro_completed = true;
				}
			} if(deltatime >= show_press_any_key_timestamp){
				press_any_key.gameObject.SetActive(true);
				deltatime += Time.deltaTime;
			} if(deltatime >= show_by_caio_timestamp){
				by_caio.gameObject.SetActive(true);
				deltatime += Time.deltaTime;
			} if(deltatime >= show_title_timestamp){
				title.gameObject.SetActive(true);
				deltatime += Time.deltaTime;
			} else {
				deltatime += Time.deltaTime;
			}

			if(deltatime <= wait_for_key_press){
				if(Input.anyKeyDown){
					deltatime += 2f;
				}
			}
		}
	}

	//each option in the menu has a function
	///MAIN MENU
	public void new_game(){
		new_game_panel.SetActive(true);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(false);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(false);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	public void settings(){
		new_game_panel.SetActive(false);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(true);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(false);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	public void credits(){
		new_game_panel.SetActive(false);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(false);
		credits_panel.SetActive(true);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(false);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	//NEW GAME MENU
	public void arcade(){
		//load arcade scene
		Debug.Log("Arcade");
		SceneManager.LoadScene(1);
	}

	public void story_mode(){
		//load story mode scene
		Debug.Log("Story Mode");
	}

	public void survival(){
		//load survival scene
		Debug.Log("Survival");
	}


	//GENERIC FUNCTIONS
	public void go_to_title_screen(){
		new_game_panel.SetActive(false);
		title_menu.SetActive(true);
		main_menu.SetActive(false);
		settings_panel.SetActive(false);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(false);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	public void go_to_main_menu(){
		new_game_panel.SetActive(false);
		title_menu.SetActive(false);
		main_menu.SetActive(true);
		settings_panel.SetActive(false);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(false);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	public void open_link(string link){
		Application.OpenURL(link);
	}

	public void show_select_ship_panel(){
		new_game_panel.SetActive(true);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(false);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(true);
		checkout.SetActive(false);
		trophy_room.SetActive(false);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	public void show_checkout(){
		new_game_panel.SetActive(true);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(false);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(true);
		trophy_room.SetActive(false);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	public void show_trophy_room(){
		new_game_panel.SetActive(false);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(false);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(true);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	public void show_settings_audio(){
		new_game_panel.SetActive(false);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(true);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(false);
		settings_audio.SetActive(true);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	public void show_settings_arcade(){
		new_game_panel.SetActive(false);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(true);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(false);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(true);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(false);
	}

	public void show_how_to_play(){
		new_game_panel.SetActive(false);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(false);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(false);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(true);
		reset_achievements.SetActive(false);
	}

	public void reset_achievements_Y_N(){
		new_game_panel.SetActive(false);
		title_menu.SetActive(false);
		main_menu.SetActive(false);
		settings_panel.SetActive(false);
		credits_panel.SetActive(false);
		select_ship_panel.SetActive(false);
		checkout.SetActive(false);
		trophy_room.SetActive(true);
		settings_audio.SetActive(false);
		settings_arcade.SetActive(false);
		how_to_play.SetActive(false);
		reset_achievements.SetActive(true);
	}

	public void quit_yes(){
		Application.Quit();
	}

	public void quit_no(){
		prompt_quit.SetActive(false);
	}


	public void quit_prompt(){
		prompt_quit.SetActive(true);
	}
}
