﻿using UnityEngine;
using System.Collections;

public class Boss5_aux : MonoBehaviour {
	public float speed = 10f;
	Transform target;
	bool chasing;

	public void chase(Transform t){
		chasing = true;
		target = t;
	}

	void Update(){
		if(chasing){
			transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
		}
	}
}
