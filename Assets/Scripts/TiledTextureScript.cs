﻿using UnityEngine;
using System.Collections;

public class TiledTextureScript : MonoBehaviour {
	Vector2 scale;
	Material mat;
	// Use this for initialization
	void Start () {
		scale = new Vector2(1, transform.localScale.y);
		mat = GetComponent<Renderer>().material;
		mat.SetTextureScale("_MainTex", scale);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
