﻿using UnityEngine;
using System.Collections;

public class PlayerType : MonoBehaviour {
	public int type = 0;
	// 0 - Metal
	// 1 - Electric
	// 2 - Plasma
	// 3 - Projectile
	public Material [] mat_types;
	public bool can_cycle_types = true;

	Renderer rend;
	int type_change = 0;

	void Start(){
		rend = transform.GetChild(0).GetComponent<Renderer>();
	}

	void Update(){
		if(can_cycle_types){
			if(Input.GetButtonUp("Next Type")){
				type_change = 1;
			} else {
				if(Input.GetButtonUp("Prev Type")){
					type_change = -1;
				} else {
					type_change = 0;
				}
			}
			if(type_change != 0){
				type += type_change;
				if(type < 0){
					type = 3;
				}
				if (type > 3){
					type = 0;
				}
				change_material();
			}
		}

	}

	void change_material(){
		rend.material = mat_types[type];
		//show some visual effects to tell the type change
	}

}
