﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipDescription : MonoBehaviour {
	public int ship_type = 0;
	public Text description_UI_text;
	public Checkout co;

	public void show(){
		description_UI_text.text = co.ship_info[ship_type].full_text;
	}
}
