﻿using UnityEngine;
using System.Collections;

public class GunShoot : Weapon {
	public float bullet_speed = 150f;
	public float rate_of_fire = 1f;
	public float start_delay = 0;
	public BulletPool bullet_pool;
	public bool active = false;

	Transform bullet_deploy;

	float bullet_deltatime = 0;

	void Start(){
		bullet_deploy = transform.GetChild(0).GetComponent<Transform>();
	}

	void Shoot(){
		//get a shootable bullet
		GameObject newBullet = bullet_pool.getBullet();
		if(newBullet != null){
			//set velocity and direction of bullet
			newBullet.transform.rotation = bullet_deploy.transform.rotation;
			newBullet.transform.position = bullet_deploy.transform.position;
			newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * -bullet_speed);
		} else {
			//all bullets are on screen
			return;
		}
	}

	void Update(){
		if(active){
			if(start_delay > 0){
				start_delay -= Time.deltaTime;
			} else {
				if(bullet_deltatime > rate_of_fire){
					bullet_deltatime = 0;
					Shoot();
				} else {
					bullet_deltatime += Time.deltaTime;
				}
			}
		}
	}
}
