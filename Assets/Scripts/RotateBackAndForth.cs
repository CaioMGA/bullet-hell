﻿using UnityEngine;
using System.Collections;

public class RotateBackAndForth : MonoBehaviour {
	public float spin_speed;
	public float offset = 0;
	public bool spin = true;
	Vector3 spin_axis_rotation;

	void Start(){
		spin_axis_rotation = new Vector3(
			0,
			0,
			0
		);
	}

	void Update(){
		if(spin){
			spin_axis_rotation = new Vector3(
				spin_axis_rotation.x,
				offset + spin_speed * Mathf.Sin(Time.time * 2),
				spin_axis_rotation.z
			);
			transform.localRotation = Quaternion.Euler(spin_axis_rotation);
		}
	}

}