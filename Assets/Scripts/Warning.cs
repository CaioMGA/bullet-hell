﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Warning : MonoBehaviour {

	//show Text
	//wait blinking_interval seconds
	//hide Text
	//wait blinking_interval seconds
	//repeat for message_time seconds
	public Text[] texts = new Text[4];
	bool [] elements_on_show = new bool[4];
	bool executing = false;

	float blinking_interval = 0.5f;
	float message_time = 10.5f;
	float event_deltatime = 0;
	float message_deltatime = 0;

	void Start(){
		show_warning(true, true, true, true);
	}

	void Update () {
		if(executing){
			Debug.Log(message_time + " / " + message_deltatime);
			if(message_deltatime > message_time){
				executing = false;
				hideAll();
			} else {
				message_deltatime += Time.deltaTime;

				if(event_deltatime > blinking_interval){
					event_deltatime = 0;
					for(int i = 0; i < 4; i++ ){
						if(elements_on_show[i]){
							bool stat = (texts[i].isActiveAndEnabled);
							texts[i].gameObject.SetActive(!stat);
						}
					}
				} else {
					event_deltatime += Time.deltaTime;
				}
				message_deltatime += Time.deltaTime;
			}
		}
	}

	void show_warning(bool top, bool bottom, bool left, bool right){
		executing = true;
		elements_on_show [0] = top;
		elements_on_show [1] = right;
		elements_on_show [2] = bottom;
		elements_on_show [3] = left;

		event_deltatime = 0;
		message_deltatime = 0;
	}

	void hideAll(){
		foreach (Text t in texts){
			t.gameObject.SetActive(false);
		}
	}
}
