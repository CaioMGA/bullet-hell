﻿using UnityEngine;
using System.Collections;

public class PlayerLogic : MonoBehaviour {
	public Transform deploy_start;
	public Transform deploy_finish;
	public float speed = 3f;

	int deaths;
	int this_session_deaths = 0;

	bool deployed = false;
	bool deploying = false;
	bool invencible = false;

	Animator anim;

	PlayerShooting ps;
	PlayerMovement pm;
	PlayerLife pl;
	Collider col;


	void Start(){
		deaths = PlayerPrefs.GetInt("Deaths");
		ps = transform.GetChild(1).GetComponent<PlayerShooting>(); // Shooter GameObject
		pm = GetComponent<PlayerMovement>();
		pl = transform.GetChild(0).transform.GetChild(0).GetComponent<PlayerLife>(); // Core GameObject
		anim = GetComponent<Animator>();
		col = GetComponent<Collider>();
		deploy_start = GameObject.FindGameObjectWithTag("Deploy Start").transform;
		deploy_finish = GameObject.FindGameObjectWithTag("Deploy Finish").transform;
		Enter();
	}

	void Update(){
		if(deploying){
			if(transform.position == deploy_finish.position){
				deploying = false;
				deployed = true;
				ps.enabled = true;
				pm.enabled = true;
				col.enabled = true;
			} else {
				transform.position = Vector3.MoveTowards(
					transform.position,
					deploy_finish.position,
					speed * Time.deltaTime);
			}
		}
		if(pl.damage_registered){
			pl.damage_registered = false;
			deaths += 1;
			this_session_deaths += 1;
			if(deaths == 20){
				GameObject.FindGameObjectWithTag("GameLogic").GetComponent<AchievementSystem>().register_achievement(10);
			}
			PlayerPrefs.SetInt("Deaths", deaths);
			Enter();
		}

		if(pl.invencible == false && invencible == true){
			invencible = false;
			anim.SetTrigger("IDLE");
		}
	}

	public void Enter(){
		deploying = true;
		deployed = false;
		invencible = true;
		transform.position = deploy_start.position;
		ps.enabled = false;
		pm.enabled = false;
		transform.rotation = Quaternion.Euler(0, 0, 0);
		anim.SetTrigger("INVENCIBLE");
		col.enabled = false;
	}

	public bool no_deaths(){
		if(this_session_deaths == 0){
			return true;
		}
		return false;
	}
}
