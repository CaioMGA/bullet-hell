﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicController : MonoBehaviour {
	public AudioMixer audio_mixer;
	bool music_muted = false;
	bool sound_muted = false;


	public void change_music_lvl(float value){
		if(!music_muted){
			audio_mixer.SetFloat("musicLvl", value);
		}
		PlayerPrefs.SetFloat("Music Vol", value);
	}
		
	public void change_sound_lvl(float value){
		if(!sound_muted){
			audio_mixer.SetFloat("soundLvl", value);
		}
		PlayerPrefs.SetFloat("Sound Vol", value);
	}

	public void mute_music(bool value){
		music_muted = value;
		if(value){ //mute
			audio_mixer.SetFloat("musicLvl", -80);
		} else { //unmute
			audio_mixer.SetFloat("musicLvl", PlayerPrefs.GetFloat("Music Vol"));
		}
	}

	public void mute_sound(bool value){
		sound_muted = value;
		if(value){ //mute
			audio_mixer.SetFloat("soundLvl", -80);
		} else { //unmute
			audio_mixer.SetFloat("soundLvl", PlayerPrefs.GetFloat("Music Vol"));
		}
	}
}
