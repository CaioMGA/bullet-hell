﻿using UnityEngine;
using System.Collections;

public class TextureAnimation : MonoBehaviour {
	public Texture [] frames;
	public float frame_interval = 1f;
	float frame_deltatime;
	int frame_index;
	int frame_count;
	public Material mat;

	void Start () {
		mat = GetComponent<Renderer>().material;
		mat.SetTexture("_MainTex", frames[frame_index]);
		frame_deltatime = 0;
		frame_index = 0;
		frame_count = frames.Length;
	}
	
	void Update () {
		if(frame_deltatime >= frame_interval){
			frame_deltatime = 0;
			if(frame_index < frame_count - 1){
				frame_index += 1;

			} else {
				frame_index = 0;
			}
			mat.SetTexture("_MainTex", frames[frame_index]);
		} else {
			frame_deltatime += Time.deltaTime;
		}
	}
}
