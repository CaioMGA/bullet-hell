﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class GameLogic : MonoBehaviour {
	public GameObject player;
	public aBoss [] bosses;
	public BulletPool [] bullet_pools;
	public ExplosionPool explosions;
	public AudioMixer mixer;
	public int boss_index = -1; //public for testing
	CreditRoll credits;

	bool gameover = false;

	AudioMixerSnapshot [] snapshots;
	PlayerShipDeploy player_ship_deploy;


	void Start () {
		credits = GetComponent<CreditRoll>();
		snapshots = new AudioMixerSnapshot[3];
		snapshots[0] = mixer.FindSnapshot("Boss Health 100%");
		snapshots[1] = mixer.FindSnapshot("Boss Health 50%");
		snapshots[2] = mixer.FindSnapshot("Boss Health 25%");
		player_ship_deploy = GetComponent<PlayerShipDeploy>();
		player_ship_deploy.enabled = true;
		player_ship_deploy.init();
		next_boss();
	}

	void Update(){
		if(bosses[boss_index].life.current_life <= 0){
			if(boss_index < 4 && bosses[boss_index].ready_to_destroy){
				next_boss();
			} else {
				//show game over screen
				//You Beat the game!
				if(player.GetComponent<PlayerLogic>().no_deaths()){
					GetComponent<AchievementSystem>().register_achievement(6);
				}
				if(!gameover){
					credits.show_game_over_screen();
					gameover = true;
					bosses[4].gameObject.SetActive(false);
				}

			}

		}
	}
	
	public void change_music(int index){
		//unmute index chanel
		//mute all others
		// 0 = Boss Health at 100%
		// 1 = Boss Health at 50%
		// 2 = Boss Health at 25%
		snapshots[index].TransitionTo(0.01f);
	}

	public void kill_all_bullets(){
		int qtd_bullet_pools = bullet_pools.Length;
		explosions.explosion_index = 0;
		foreach(BulletPool bp in bullet_pools){
			int pool_bullets = bp.qtd_bullets;
			for(int j = 0; j < pool_bullets; j++){
				if(bp.active_bullets[j] == false){

				} else {
					GameObject newExplosion = explosions.get_explosion();
					newExplosion.transform.position = bp.transform.GetChild(j).transform.position;
					bp.deactivate_bullet(j);
					//Debug.Log("Explode");
				}
			}
		}
	}

	public void damage_all_enemies(float damage){
		foreach (aWeapon w in bosses[boss_index].weapons){
			w.life.current_life -= damage;
		}
	}

	public void next_boss(){
		boss_index += 1;
		if(boss_index < bosses.Length){
			bosses[boss_index].gameObject.SetActive(true);
			bosses[boss_index].enter();
			bosses[boss_index -1].gameObject.SetActive(false);
			if(boss_index == 4){ //hydra
				player.GetComponent<PlayerSpecial>().hydra_boss(bosses[boss_index].GetComponentInChildren<aB5Core>());
			}
		}
	}
}
