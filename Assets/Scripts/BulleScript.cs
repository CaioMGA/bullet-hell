﻿using UnityEngine;
using System.Collections;

public class BulleScript : MonoBehaviour {

	public int bullet_index = -1;

	void OnTriggerEnter(Collider other){
		if(other.transform.CompareTag("Out Of Bounds")){
			recycle();
		}
	}

	public void recycle(){
		transform.parent.GetComponent<BulletPool>().deactivate_bullet(bullet_index);
	}
}
