﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Checkout : MonoBehaviour {
	public int input_type;
	public int ship_type;
	public ShipInfo [] ship_info;
	public Text stats_board;
	public Image input_icon;
	public Image ship_icon;
	public Sprite [] input_icons;


	public void keyboard(){
		PlayerPrefs.SetInt("Input Type", 0);
		input_type = 0;
	}

	public void joystick(){
		PlayerPrefs.SetInt("Input Type", 1);
		input_type = 1;
	}

	public void shipA(){
		PlayerPrefs.SetInt("Ship Type", 0);
		ship_type = 0;
		create_stats_board();
	}

	public void shipB(){
		PlayerPrefs.SetInt("Ship Type", 1);
		ship_type = 1;
		create_stats_board();
	}

	public void shipC(){
		PlayerPrefs.SetInt("Ship Type", 2);
		ship_type = 2;
		create_stats_board();
	}

	void create_stats_board(){
		string stats = "";
		stats += ship_info[ship_type].generate_speed_bar("| | ");
		stats += "\n";
		stats += ship_info[ship_type].generate_handling_bar("| | ");
		stats += "\n";
		stats += ship_info[ship_type].dificulty;
		stats_board.text = stats;
		ship_icon.sprite = ship_info[ship_type].icon;
		input_icon.sprite = input_icons[input_type];
		Debug.Log("Stats Board created");
	}
}
