﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreditRoll : MonoBehaviour {
	public float you_win_timer = 5f;
	public float fade_out_timer = 5f;

	public Image fade_panel;
	public GameObject you_win;

	float deltatime = 0;

	bool you_win_phase = false;
	bool fade_out_phase = false;

	void Update(){
		if(fade_out_phase){
			if(deltatime > fade_out_timer){
				SceneManager.LoadScene(0);
			} else {
				deltatime += Time.deltaTime;
				fade_out();
			}
		} else if(you_win_phase){
			if(deltatime > fade_out_timer){
				you_win_phase = false;
				fade_out_phase = true;
				deltatime = 0;
			} else {
				deltatime += Time.deltaTime;
			}
		}
	}

	public void show_game_over_screen(){
		you_win.SetActive(true);
		you_win_phase = true;
	}

	void fade_out(){
		fade_panel.color = new Color(fade_panel.color.r, fade_panel.color.g, fade_panel.color.b, fade_panel.color.a + (0.2f * Time.deltaTime));
	}
}
