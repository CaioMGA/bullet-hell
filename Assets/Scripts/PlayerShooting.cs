﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour {

	public float []bullet_interval;
	public float []bullet_speed;
	public BulletPool [] bullet_pools;
	public int input_type = 0;
	public int ship_type;

	Transform []bullet_deploy;
	Vector3 inputs;
	Vector3 raw_inputs;
	float rotation;
	float bullet_deltatime = 0;

	public int [] projectile_spread;
	public int [] plasma_spread;
	public int [] metal_spread;
	int [] bullet_spread_index;
	int []bullet_spread_length;
	AudioSource audioSource;
	PlayerType type;


	void Start(){
		bullet_spread_length = new int[4];
		bullet_spread_index = new int[4];
		for(int i = 0; i < 4; i++){
			bullet_spread_index[i] = 0;
			if(i == 0){
				bullet_spread_length[i] = metal_spread.Length;
			} else if(i == 1){
				//electric, do nothing
			} else if(i == 2){
				bullet_spread_length[i] = plasma_spread.Length;
			} else if(i == 3){
				bullet_spread_length[i] = projectile_spread.Length;
			}
		}

		int child_count = transform.childCount;
		bullet_deploy  = new Transform[child_count];
		for(int i = 0; i< child_count; i++){
			bullet_deploy[i] = transform.GetChild(i);
		}
		audioSource  = GetComponentInParent<AudioSource>();
		audioSource.mute = true;
		type = GetComponentInParent<PlayerType>();
		input_type = PlayerPrefs.GetInt("Input Type");

		init_bullet_pool();
		ship_type = PlayerPrefs.GetInt("Ship Type");
	}

	void get_inputs(){
		if(input_type == 0){
			float aim_x = 0;
			float aim_y = 0;
			if(Input.GetKey(KeyCode.LeftArrow)){
				aim_x -= 1;
			}
			if(Input.GetKey(KeyCode.RightArrow)){
				aim_x += 1;
			}
			if(Input.GetKey(KeyCode.UpArrow)){
				aim_y += 1;
			}
			if(Input.GetKey(KeyCode.DownArrow)){
				aim_y -= 1;
			}

			inputs = new Vector3(aim_x, aim_y, 0f);
			raw_inputs = new Vector2(aim_x, aim_y);
		}
		else if (input_type == 1){
			inputs = new Vector3(Input.GetAxis("Horizontal2"), Input.GetAxis("Vertical2"), 0f);
			raw_inputs = new Vector2(Input.GetAxisRaw("Horizontal2"), Input.GetAxisRaw("Vertical2"));
		}
	}

	void calculate_rotation(){
		if(inputs.sqrMagnitude < 0.01f){
			audioSource.mute = true;
			return;
		}
		rotation = Mathf.Atan2(inputs.x, inputs.y) * Mathf.Rad2Deg;
		audioSource.mute = false;

	}

	void rotate_me(){
		transform.rotation = Quaternion.Euler(0, rotation, 0);
	}


	void Shoot(){
		GameObject newBullet;
		if(type.type != 1){ // not electric
			int [] spread;
			if(type.type == 2){
				spread = plasma_spread;
			} else if(type.type == 3){
				spread = projectile_spread;
			} else {
				spread = metal_spread;
			}
			switch(spread[bullet_spread_index[type.type]]){
			case 0:
				//single shot
				newBullet = bullet_pools[type.type].getBullet();
				if(newBullet != null){
					//set velocity and direction of bullet
					newBullet.transform.rotation = bullet_deploy[0].transform.rotation;
					newBullet.transform.position = bullet_deploy[0].transform.position;
					newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed[type.type]);
				}
				break;
			case 1:
				//two bullets
				newBullet = bullet_pools[type.type].getBullet();
				if(newBullet != null){
					//set velocity and direction of bullet
					newBullet.transform.rotation = bullet_deploy[3].transform.rotation;
					newBullet.transform.position = bullet_deploy[3].transform.position;
					newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed[type.type]);
				}
				newBullet = bullet_pools[type.type].getBullet();
				if(newBullet != null){
					//set velocity and direction of bullet
					newBullet.transform.rotation = bullet_deploy[4].transform.rotation;
					newBullet.transform.position = bullet_deploy[4].transform.position;
					newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed[type.type]);
				}
				break;
			case 2:
				//3 bullets
				newBullet = bullet_pools[type.type].getBullet();
				if(newBullet != null){
					//set velocity and direction of bullet
					newBullet.transform.rotation = bullet_deploy[0].transform.rotation;
					newBullet.transform.position = bullet_deploy[0].transform.position;
					newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed[type.type]);
				}
				newBullet = bullet_pools[type.type].getBullet();
				if(newBullet != null){
					//set velocity and direction of bullet
					newBullet.transform.rotation = bullet_deploy[5].transform.rotation;
					newBullet.transform.position = bullet_deploy[5].transform.position;
					newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed[type.type]);
				}
				newBullet = bullet_pools[type.type].getBullet();
				if(newBullet != null){
					//set velocity and direction of bullet
					newBullet.transform.rotation = bullet_deploy[6].transform.rotation;
					newBullet.transform.position = bullet_deploy[6].transform.position;
					newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed[type.type]);
				}
				break;
			case 3:
				//2 bullets in angle
				newBullet = bullet_pools[type.type].getBullet();
				if(newBullet != null){
					//set velocity and direction of bullet
					newBullet.transform.rotation = bullet_deploy[1].transform.rotation;
					newBullet.transform.position = bullet_deploy[1].transform.position;
					newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed[type.type]);
				}
				newBullet = bullet_pools[type.type].getBullet();
				if(newBullet != null){
					//set velocity and direction of bullet
					newBullet.transform.rotation = bullet_deploy[2].transform.rotation;
					newBullet.transform.position = bullet_deploy[2].transform.position;
					newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed[type.type]);
				}
				break;
			default:
				Debug.Log("Spread Type incorrect");
				break;
			}
			if(bullet_spread_index[type.type] >= bullet_spread_length[type.type] -1){
				bullet_spread_index[type.type] = 0;
			} else {
				bullet_spread_index[type.type] += 1;
			}
		} else if(type.type == 1){
			newBullet = bullet_pools[type.type].getBullet();
			if(newBullet != null){
				//set velocity and direction of bullet
				newBullet.transform.rotation = bullet_deploy[0].transform.rotation;
				newBullet.transform.position = bullet_deploy[0].transform.position;
				newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed[type.type]);
			}
		}
	}

	void Update(){
		get_inputs();
		if(ship_type == 0){
			calculate_rotation();
			rotate_me();
		}
		if(raw_inputs.sqrMagnitude > 0.01f){
			if(bullet_deltatime > bullet_interval[type.type]){
				Shoot();
				bullet_deltatime = 0;
			} else {
				bullet_deltatime += Time.deltaTime;
			}
		}
	}

	void init_bullet_pool(){
		GameObject [] tmp_pools = GameObject.FindGameObjectsWithTag("Bullet Pool");
		bullet_pools = new BulletPool[4];
		foreach(GameObject bp in tmp_pools){
			if(bp.transform.name.Equals("Player Bullet Pool - Metal")){
				bullet_pools[0] = bp.GetComponent<BulletPool>();
			} else if(bp.transform.name.Equals("Player Bullet Pool - Electric")){
				bullet_pools[1] = bp.GetComponent<BulletPool>();
			} else if(bp.transform.name.Equals("Player Bullet Pool - Plasma")){
				bullet_pools[2] = bp.GetComponent<BulletPool>();
			} else if(bp.transform.name.Equals("Player Bullet Pool - Projectiles")){
				bullet_pools[3] = bp.GetComponent<BulletPool>();
			}
		}
	}
}
