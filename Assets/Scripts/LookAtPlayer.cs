﻿using UnityEngine;
using System.Collections;

public class LookAtPlayer : MonoBehaviour {
	public Transform player;

	void Start () {
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	void Update () {
		gameObject.transform.LookAt(player);
	}
}
