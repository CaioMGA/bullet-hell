﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementSystem : MonoBehaviour {
	public GameObject achievement_panel;
	public Text achievement_panel_title;
	public Text achievement_panel_description;
	public Image achievement_panel_icon;
	public Achievement [] achievements;

	public void register_achievement(int index){
		if(PlayerPrefs.GetInt("achievement_" + index) == 0){
			PlayerPrefs.SetInt("achievement_" + index, 1);
			show_achievement(index);
		}
	}

	void show_achievement(int index){
		achievement_panel_icon.sprite = achievements[index].icon;
		achievement_panel_title.text = achievements[index].title;
		achievement_panel_description.text = achievements[index].description;
		achievement_panel.SetActive(true);
	}

	public void reset_achievements(){
		for(int i = 0; i< 13; i++){
			PlayerPrefs.SetInt("achievement_" + i, 0);
		}
	}

	void Update(){
		/*
		if(Input.GetKeyDown(KeyCode.M)){
			reset_achievements();
		}
		*/
	}
}
