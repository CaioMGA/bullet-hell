﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenu : MonoBehaviour {
	public GameObject ingame_menu;
	public GameObject settings_menu;
	public GameObject quit_menu;
	public GameObject restart_menu;

	//states
	bool showing_menu = false;
	bool showing_settings = false;
	bool showing_prompt_quit = false;
	bool showing_prompt_restart = false;

	void Start () {
		
	}
	
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			if(!showing_prompt_quit && !showing_prompt_restart && !showing_settings){
				showing_menu = !showing_menu;
			}
			if(showing_prompt_restart){
				showing_prompt_restart = false;
			}

			if(showing_prompt_quit){
				showing_prompt_quit = false;
			}

			if(showing_settings){
				showing_settings = false;
			}
		}

		if(showing_menu){
			ingame_menu.SetActive(true);
			if(showing_settings){
				settings_menu.SetActive(true);
			} else {
				settings_menu.SetActive(false);
			}

			if(showing_prompt_quit){
				quit_menu.SetActive(true);
			} else {
				quit_menu.SetActive(false);
			}

			if(showing_prompt_restart){
				restart_menu.SetActive(true);
			} else {
				restart_menu.SetActive(false);
			}
		} else {
			ingame_menu.SetActive(false);
		}
	}

	public void resume(){
		showing_menu = false;
	}

	public void restart(){
		showing_prompt_restart = true;
	}

	public void restart_yes(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		Debug.Log("Restart Scene");
	}

	public void restart_no(){
		showing_prompt_restart = false;
	}

	public void settings(){
		showing_settings = true;
	}

	public void quit(){
		showing_prompt_quit = true;
	}

	public void quit_yes(){
		SceneManager.LoadScene(0);
	}

	public void quit_no(){
		showing_prompt_quit = false;
	}
}
