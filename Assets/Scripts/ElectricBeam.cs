﻿using UnityEngine;
using System.Collections;

public class ElectricBeam : Weapon {
	//attack cycle
	// idle -> telegraph -> attack -> repeat
	public Texture telegraph_frame;
	public float idle_time = 5f;
	public float telegraph_time = 2f;
	public float attack_time = 3f;
	public bool automatic_shooting = false;
	public AudioClip telegraph_fx;
	public AudioClip attack_fx;

	float animation_deltatime;
	BoxCollider collider;
	string animation_state;
	//animation states:
	// idle
	// telegraph
	// attack
	GameObject beam;
	TextureAnimation animation;
	AudioSource audioSource;

	void Start () {
		animation = transform.GetChild(0).GetComponent<TextureAnimation>();
		animation_deltatime = 0f;
		beam = transform.GetChild(0).gameObject;
		collider = beam.GetComponent<BoxCollider>();
		audioSource = GetComponent<AudioSource>();
		show_idle();
	}
	

	void Update () {
		if(automatic_shooting){ // if automatic shooting then shoot ass soon as possible
			if(animation_state.Equals("idle")){
				if(animation_deltatime >= idle_time){
					show_telegraph();
				} else {
					animation_deltatime += Time.deltaTime;
				}
			}
		}

		if(animation_state.Equals("telegraph")){
			if(animation_deltatime >= telegraph_time){
				show_attack();
			} else {
				animation_deltatime += Time.deltaTime;
			}
		}

		if(animation_state.Equals("attack")){
			if(animation_deltatime >= attack_time){
				show_idle();
			} else {
				animation_deltatime += Time.deltaTime;
			}
		}
	}

	public void show_idle(){
		beam.SetActive(false);
		animation.enabled = false;
		animation_deltatime = 0;
		animation_state = "idle";
		audioSource.mute = true;
		audioSource.Stop();
	}

	void show_telegraph(){
		beam.SetActive(true);
		animation.enabled = true;
		animation.mat.SetTexture("_MainTex", telegraph_frame);
		animation.enabled = false;
		animation_deltatime = 0;
		collider.enabled = false;
		animation_state = "telegraph";
		audioSource.clip = telegraph_fx;
		audioSource.mute = false;
		audioSource.Play();

	}

	void show_attack(){
		beam.SetActive(true);
		animation.enabled = true;
		animation_deltatime = 0;
		collider.enabled = true;
		animation_state = "attack";
		audioSource.clip = attack_fx;
		audioSource.mute = false;
		audioSource.Play();
	}

	void shoot(){
		show_telegraph();
	}
}
