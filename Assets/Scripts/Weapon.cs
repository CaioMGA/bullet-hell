﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
	public bool weapon_active = true;
	public void explode(){
		int child_count = transform.childCount;
		for(int i = 0; i < child_count; i++){
			transform.GetChild(i).gameObject.SetActive(false);
		}
		weapon_active = false;
	}
}
