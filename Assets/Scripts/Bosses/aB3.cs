﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aB3 : aBoss {

	public float boss_deploy_speed = 1.5f;
	public float phase_transition_time = 3f;

	float [] phases;
	int current_phase;
	aBossMovement movement_phase_1_and_2;
	aBossMovement movement_phase_3;
	float phase_transition_deltatime;

	aWeapon arc_shooter;
	SpinForever spin;

	//states
	bool entering = false;
	bool filling_lifebar = false;
	bool growling = false;
	bool fighting = false;
	bool between_phases = false;
	bool spinning = false;

	void Awake() {
		init();
		arc_shooter = transform.GetChild(0).GetComponent<aWeapon>();
		life.total_life += arc_shooter.life.total_life;
		life.current_life = life.total_life;
		arc_shooter.life.current_life = arc_shooter.life.total_life;
		phases = new float[]{life.total_life * 0.75f, life.total_life * 0.5f, life.total_life * 0.25f, 0};
		current_phase = 0;
		aBossMovement [] mov = GetComponents<aBossMovement>();
		if(mov[0].path.Length > mov[1].path.Length){ //second phase has more steps
			movement_phase_1_and_2 = mov[1];
			movement_phase_3 = mov[0];
		} else {
			movement_phase_1_and_2 = mov[0];
			movement_phase_3 = mov[1];
		}
		spin = GetComponent<SpinForever>();
		enter();
	}

	void Update(){
		update_life();
		if(life.current_life <= 0){
			gl.GetComponent<AchievementSystem>().register_achievement(2);
			ready_to_destroy = true;
		}
		life.update_lifebar();
		if(entering){
			transform.position = Vector3.MoveTowards(transform.position, boss_deploy.position, Time.deltaTime * boss_deploy_speed);
			if(transform.position == boss_deploy.position){
				entering = false;
				fill_lifebar();
			}
		} else if(filling_lifebar){
			filling_lifebar = life.filling_lifebar;
			if(!filling_lifebar){
				growling = true;
				growl.growl();
				cam_shake.shake(1, 3);
			}
		} else if(growling){
			growling = growl.growling;
			if(!growling){
				fighting = true;
				say_my_name();
			}
		} else if(fighting){
			if(current_phase == 0){ //just shoot
				if(!weapons_active){
					activate_all_weapons_local();
				}
			} else if(current_phase == 1){ // shoot and move
				if(!weapons_active){
					activate_all_weapons_local();
				}
				movement_phase_1_and_2.move();
			} else if(current_phase == 2){ // shoot and move and arc shooter
				if(!weapons_active){
					activate_all_weapons_local();
				}
				movement_phase_1_and_2.move();
				arc_shooter.gameObject.SetActive(true);
			}

			else if(current_phase == 3){ // shoot and move and arc shooter and rotate
				if(!weapons_active){
					activate_all_weapons_local();
				}
				movement_phase_3.move();
				if(!spinning){
					spin.enabled = true;
					spinning = true;
				}

			}

			if(life.current_life >= phases[current_phase]){

			} else {
				current_phase += 1;
				between_phases = true;
				fighting = false;
				phase_transition_deltatime = 0;
				deactivate_all_weapons_local();
				gl.kill_all_bullets();
				cam_shake.shake(0.5f, phase_transition_time - 0.5f);
			}

		} if(between_phases){
			if(phase_transition_deltatime >= phase_transition_time){
				fighting = true;
				between_phases = false;
				activate_all_weapons_local();
			} else {
				phase_transition_deltatime += Time.deltaTime;
			}
		}
	}

	public override void enter ()
	{
		entering = true;
	}

	void fill_lifebar(){
		life.fill_lifebar();
		filling_lifebar = true;
	}

	void update_life(){
		float tmp_life = 0;
		foreach(aWeapon w in weapons){
			tmp_life += w.life.current_life;
		}
		tmp_life += arc_shooter.life.current_life;
		life.current_life = tmp_life;
	}

	void deactivate_all_weapons_local(){
		if(current_phase > 2){
			arc_shooter.stop();
		}
		deactivate_all_weapons();
	}

	void activate_all_weapons_local(){
		if(current_phase > 2){
			arc_shooter.resume();
		}
		activate_all_weapons();
	}
}
