﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aGrowl : MonoBehaviour {
	public float growl_duration = 1f;
	public AudioClip growl_snd;

	public bool growling = false;
	AudioSource audio_source;
	CameraShake cam_shake;
	float growl_elapsed_time;

	void Start(){
		audio_source = GetComponent<AudioSource>();
		cam_shake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShake>();
	}

	void Update(){
		if(growling){
			if(growl_elapsed_time >= growl_duration){
				growling = false;
			} else {
				growl_elapsed_time += Time.deltaTime;
			}
		}
	}

	public void growl(){
		growling = true;
		growl_duration = growl_snd.length;
		growl_elapsed_time = 0;
		audio_source.PlayOneShot(growl_snd);
		cam_shake.shake(1f, growl_duration - 1f, 0.5f, _fadein:true);

	}
}
