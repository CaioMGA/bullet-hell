﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aB5Core : MonoBehaviour {
	// when the Boss's Core is hit EVERY weapon get damaged.
	// The damage is  [(standard damage value * 120%) / Boss weapons count]
	// It is quicker to defeat a Boss hitting only its core

	GameLogic gl;
	float damage;
	GameObject shield;
	public aBossLife life;

	bool invencible = true;
	bool showing_shield = true;

	void Start(){
		gl = GameObject.FindGameObjectWithTag("GameLogic").GetComponent<GameLogic>();
		damage = 0.12f / GetComponentInParent<aBoss>().weapons.Length;
		shield = transform.GetChild(0).gameObject;
		life = GetComponentInParent<aBossLife>();
	}

	void Update(){
		if(invencible){
			if(!showing_shield){
				shield.SetActive(true);
				showing_shield = true;
			}
		} else {
			if(showing_shield){
				shield.SetActive(false);
				showing_shield = false;
			}
		}

	}

	void OnTriggerEnter(Collider other){
		if(other.transform.CompareTag("HERO Bullet")){
			other.GetComponentInParent<BulleScript>().recycle();
			if(!invencible){
				life.current_life -= 0.12f;
			}
		}
	}

	public void set_invencibility(bool value){
		invencible = value;
	}
}
