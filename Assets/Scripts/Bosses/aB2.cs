﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aB2 : aBoss {

	public float boss_deploy_speed = 1.5f;
	public float phase_transition_time = 3f;

	float [] phases;
	int current_phase;
	aBossMovement movement_phase_1;
	aBossMovement movement_phase_2;

	float phase_transition_deltatime;

	//states
	bool entering = false;
	bool filling_lifebar = false;
	bool growling = false;
	bool fighting = false;
	bool between_phases = false;

	void Awake() {
		init();
		phases = new float[]{life.total_life * 0.75f, life.total_life * 0.25f, 0};
		current_phase = 0;
		aBossMovement [] mov = GetComponents<aBossMovement>();
		if(mov[0].path.Length > mov[1].path.Length){ //second phase has more steps
			movement_phase_1 = mov[1];
			movement_phase_2 = mov[0];
		} else {
			movement_phase_1 = mov[0];
			movement_phase_2 = mov[1];
		}
		enter();
	}

	void Update(){
		update_life();
		if(life.current_life <= 0){
			gl.GetComponent<AchievementSystem>().register_achievement(1);
			ready_to_destroy = true;
		}
		life.update_lifebar();
		if(entering){
			transform.position = Vector3.MoveTowards(transform.position, boss_deploy.position, Time.deltaTime * boss_deploy_speed);
			if(transform.position == boss_deploy.position){
				entering = false;
				fill_lifebar();
			}
		} else if(filling_lifebar){
			filling_lifebar = life.filling_lifebar;
			if(!filling_lifebar){
				growling = true;
				growl.growl();
				cam_shake.shake(1, 3);
			}
		} else if(growling){
			growling = growl.growling;
			if(!growling){
				fighting = true;
				say_my_name();
			}
		} else if(fighting){
			if(current_phase == 0){ //just shoot
				if(!weapons_active){
					activate_all_weapons();
				}
			} else if(current_phase == 1){ // shoot and move
				if(!weapons_active){
					activate_all_weapons();
				}
				movement_phase_1.move();
			} else if(current_phase == 2){ // shoot and move
				if(!weapons_active){
					activate_all_weapons();
				}
				movement_phase_2.move();
			}

			if(life.current_life >= phases[current_phase]){

			} else {
				current_phase += 1;
				between_phases = true;
				fighting = false;
				phase_transition_deltatime = 0;
				deactivate_all_weapons();
				gl.kill_all_bullets();
				cam_shake.shake(0.5f, phase_transition_time - 0.5f);
			}

		} if(between_phases){
			if(phase_transition_deltatime >= phase_transition_time){
				fighting = true;
				between_phases = false;
				activate_all_weapons();
			} else {
				phase_transition_deltatime += Time.deltaTime;
			}
		}
	}

	public override void enter ()
	{
		entering = true;
	}

	void fill_lifebar(){
		life.fill_lifebar();
		filling_lifebar = true;
	}

	void update_life(){
		float tmp_life = 0;
		foreach(aWeapon w in weapons){
			tmp_life += w.life.current_life;
		}
		life.current_life = tmp_life;
	}
}
