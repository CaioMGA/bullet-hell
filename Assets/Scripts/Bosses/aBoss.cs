﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class aBoss : MonoBehaviour {
	public aWeapon [] weapons;
	public string alias = "name";
	public Transform boss_deploy;

	public aBossLife life;
	protected aGrowl growl;
	protected bool weapons_active = false;
	protected CameraShake cam_shake;
	protected GameLogic gl;
	private Text boss_text;

	public bool ready_to_destroy = false;

	void Update(){
		
	}

	protected void init(){
		life = GetComponent<aBossLife>();
		if(life == null){
			life = gameObject.AddComponent<aBossLife>();
		}
		life.total_life = 0;
		weapons = GetComponentsInChildren<aWeapon>();
		deactivate_all_weapons();
		foreach(aWeapon w in weapons){
			life.total_life += w.life.total_life;
		}
		life.current_life = life.total_life;
		growl = GetComponent<aGrowl>();
		cam_shake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShake>();
		gl = GameObject.FindGameObjectWithTag("GameLogic").GetComponent<GameLogic>();
		boss_text = GameObject.FindGameObjectWithTag("Boss Name").GetComponent<Text>();
	}

	public void say_my_name(){
		boss_text.text = alias;
	}

	public virtual void enter(){
		Debug.Log("if you read this you did something wrong in your boss class");
	}

	public void deactivate_all_weapons(){
		foreach(aWeapon w in weapons){
			w.stop();
		}
		weapons_active = false;
	}

	public void activate_all_weapons(){
		foreach(aWeapon w in weapons){
			w.resume();
		}
		weapons_active = true;
	}
}
