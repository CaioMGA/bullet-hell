﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aBossMovement : MonoBehaviour {
	public Transform [] path;
	public float speed = 2f;
	int path_index;
	float path_length;

	void Start(){
		path_length = path.Length;
	}


	public void move(){
		transform.position = Vector3.MoveTowards(transform.position, path[path_index].position, speed * Time.deltaTime);
		if(transform.position == path[path_index].position){
			path_index += 1;
			if(path_index >= path_length){
				path_index = 0;
			}
		}
	}

	public void move(float movement_speed){
		transform.position = Vector3.MoveTowards(transform.position, path[path_index].position, movement_speed * Time.deltaTime);
		if(transform.position == path[path_index].position){
			path_index += 1;
			if(path_index >= path_length){
				path_index = 0;
			}
		}
	}
}
