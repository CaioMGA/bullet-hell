﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aB4 : aBoss {

	public float boss_deploy_speed = 1.5f;
	public float phase_transition_time = 3f;
	public float spin_interval_phase_0_and_1 = 2f;
	public float spin_interval_phase_2 = 1.5f;
	public float spin_interval_phase_3 = 1.2f;
	aWeapon [] arc_shooters; // arc shooters array
	//arc shooters begin hidden, then they become visible 

	float [] phases;
	int current_phase;
	float phase_transition_deltatime;
	float spin_deltatime = 0;

	LookAtPlayer boss_lookAtPlayer;
	LookAtPlayer [] electric_weapons_lookAtPlayer;
	LookAtPlayer [] projectile_weapons_lookAtPlayer;
	aWeaponSpin [] weapon_spins;
	aChase chase;
	aBossCore core;

	//states
	bool entering = false;
	bool filling_lifebar = false;
	bool growling = false;
	bool fighting = false;
	bool between_phases = false;
	bool chasing = false;
	bool showing_arc_shooters = false;
	bool projectile_weapons_looking_at_player = false;
	bool electric_weapons_looking_at_player = false;
	bool boss_looking_at_player = false;

	void Awake() {
		init();
		init_look_at_player_scripts();
		init_arc_shooters();
		init_weapon_spins();
		init_chase();
		core = GetComponentInChildren<aBossCore>();
		//life.current_life = life.total_life;
		phases = new float[]{life.total_life * 0.75f, life.total_life * 0.5f, life.total_life * 0.25f, 0};
		current_phase = 0;
		enter();
	}

	void init_arc_shooters(){
		aArcShooter [] tmp_arc = GetComponentsInChildren<aArcShooter>();
		arc_shooters = new aWeapon[tmp_arc.Length];
		int arc_index = 0;
		foreach(aArcShooter aas in tmp_arc){
			arc_shooters[arc_index] = aas.transform.GetComponent<aWeapon>();
			//life.total_life += arc_shooters[arc_index].life.total_life;
			//arc_shooters[arc_index].life.current_life = arc_shooters[arc_index].life.total_life;
			arc_index++;
			aas.gameObject.SetActive(false);
		}
	}

	void init_look_at_player_scripts(){
		boss_lookAtPlayer = GetComponent<LookAtPlayer>();
		LookAtPlayer [] tmp = GetComponentsInChildren<LookAtPlayer>();
		int electric_count = 0;
		int projectile_count = 0;
		foreach(LookAtPlayer a in tmp){
			if(a.GetComponentInChildren<aElectricBeamShooter>() != null && a.GetComponent<aBoss>() == null){
				electric_count ++;
			} else {
				if(a.GetComponent<aBoss>() == null){
					projectile_count ++;
				}
			}
		}
		electric_weapons_lookAtPlayer = new LookAtPlayer[electric_count];
		projectile_weapons_lookAtPlayer = new LookAtPlayer[projectile_count];

		int electric_index = 0;
		int projectile_index = 0;
		foreach(LookAtPlayer b in tmp){
			if(b.GetComponentInChildren<aElectricBeamShooter>() != null && b.GetComponent<aBoss>() == null) {
				electric_weapons_lookAtPlayer[electric_index] = b;
				electric_index++;
			} else {
				if(b.GetComponent<aBoss>() == null){
					projectile_weapons_lookAtPlayer[projectile_index] = b;
					projectile_index++;
				}
			}
		}
	}

	void init_weapon_spins(){
		weapon_spins = GetComponentsInChildren<aWeaponSpin>();
	}

	void init_chase(){
		chase = GetComponent<aChase>();
		chase.target = GameObject.FindGameObjectWithTag("Player").transform;
	}

	void Update(){
		if(life.current_life <= 0){
			gl.GetComponent<AchievementSystem>().register_achievement(3);
			ready_to_destroy = true;
		}
		if(entering){
			transform.position = Vector3.MoveTowards(transform.position, boss_deploy.position, Time.deltaTime * boss_deploy_speed);
			if(transform.position == boss_deploy.position){
				entering = false;
				fill_lifebar();
			}
		} else if(filling_lifebar){
			filling_lifebar = life.filling_lifebar;
			if(!filling_lifebar){
				growling = true;
				growl.growl();
				cam_shake.shake(1, growl.growl_duration - 0.5f);
			}
		} else if(growling){
			growling = growl.growling;
			if(!growling){
				fighting = true;
				say_my_name();
			}
		} else if(fighting){
			update_life();
			life.update_lifebar();
			if(current_phase == 0){ // shoot and spin weapons
				if(!weapons_active){
					activate_all_weapons_local();
				}
				spin_weapons();
			} else if(current_phase == 1){ // shoot and spin weapons and projectile weapons look at player
				if(!weapons_active){
					activate_all_weapons_local();
				}
				if(!projectile_weapons_looking_at_player){
					projectile_weapons_looking_at_player = true;
					activate_projectile_lookAtPlayer();
				}
				spin_weapons();
			} else if(current_phase == 2){ // shoot and spin weapons and projectile weapons look at player and Boos looks at player
				if(!weapons_active){
					activate_all_weapons_local();
				}

				if(!showing_arc_shooters){
					showing_arc_shooters = true;
					show_arc_shooters();
				}
				spin_weapons();
			}

			else if(current_phase == 3){ // shoot and spin weapons and projectile weapons look at player and chases player FASTER and electric beams aim at player
				if(!weapons_active){
					activate_all_weapons_local();

				}
				if(!electric_weapons_looking_at_player){
					electric_weapons_looking_at_player = true;
					activate_electric_lookAtPlayer();
				}
				if(!chasing){
					chasing = true;
					chase.enabled = true;
				}
				if(!boss_looking_at_player){
					boss_looking_at_player = true;
					boss_lookAtPlayer.enabled = true;
				}
				spin_weapons();
			}

			if(life.current_life >= phases[current_phase]){

			} else {
				current_phase += 1;
				between_phases = true;
				fighting = false;
				phase_transition_deltatime = 0;
				deactivate_all_weapons_local();
				gl.kill_all_bullets();
				cam_shake.shake(0.5f, phase_transition_time - 0.5f);
				Debug.Log(current_phase);
			}

		} if(between_phases){
			if(phase_transition_deltatime >= phase_transition_time){
				fighting = true;
				between_phases = false;
				activate_all_weapons_local();
			} else {
				phase_transition_deltatime += Time.deltaTime;
			}
		}
	}

	public override void enter ()
	{
		entering = true;
	}

	void fill_lifebar(){
		life.fill_lifebar();
		filling_lifebar = true;
	}

	void update_life(){
		float tmp_life = 0;
		foreach(aWeapon w in weapons){
			tmp_life += w.life.current_life;
		}
		if(!showing_arc_shooters){
			tmp_life += (20 * arc_shooters.Length);
		}

		life.current_life = tmp_life;
	}

	void deactivate_all_weapons_local(){
		core.set_invencibility(true);
		deactivate_all_weapons();
	}

	void activate_all_weapons_local(){
		core.set_invencibility(false);
		activate_all_weapons();
	}

	void spin_weapons(){
		if(current_phase <= 1 ){
			if(spin_interval_phase_0_and_1 >= spin_deltatime){
				spin_deltatime += Time.deltaTime;
			} else {
				foreach(aWeaponSpin a in weapon_spins){
					a.spin();
				}
				spin_deltatime = 0;
			}
		}

		if(current_phase == 2){
			if(spin_interval_phase_2 >= spin_deltatime){
				spin_deltatime += Time.deltaTime;
			} else {
				foreach(aWeaponSpin a in weapon_spins){
					a.spin();
				}
				spin_deltatime = 0;
			}
		}

		if(current_phase == 3){
			if(spin_interval_phase_3 >= spin_deltatime){
				spin_deltatime += Time.deltaTime;
			} else {
				foreach(aWeaponSpin a in weapon_spins){
					a.spin();
				}
				spin_deltatime = 0;
			}
		}
	}

	void activate_projectile_lookAtPlayer(){
		foreach(LookAtPlayer l in projectile_weapons_lookAtPlayer){
			l.enabled = true;
		}
	}

	void activate_electric_lookAtPlayer(){
		foreach(LookAtPlayer l in electric_weapons_lookAtPlayer){
			l.enabled = true;
		}
	}

	void show_arc_shooters(){
		foreach(aWeapon w in arc_shooters){
			w.gameObject.SetActive(true);
		}
		activate_all_weapons_local();
	}
}
