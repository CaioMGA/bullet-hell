﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aB5 : aBoss {

	public float boss_deploy_speed = 1.5f;
	public float phase_transition_time = 3f;
	public float head_deploy_time = 4f;

	public float neck_spin_speed_phase_1 = 5;
	public float neck_spin_speed_phase_2 = -10;
	public float neck_spin_speed_phase_3 = 15;

	aArcShooter [] arc_shooters;

	float [] phases;
	int current_phase;
	float phase_transition_deltatime;
	float head_deploy_deltatime = 0;
	aB5Core core;
	SpinForever neck_spin;
	int heads_showing = 2;
	int total_heads = 10;
	GameObject [] heads;
	HydraWeapon [] heads_script;


	//states
	bool entering = false;
	bool filling_lifebar = false;
	bool growling = false;
	bool fighting = false;
	bool between_phases = false;
	bool deploying_head = false;
	bool neck_spinning = false;
	bool plasma_arc_active = false;
	bool projectile_arc_active = false;

	void Awake() {
		init();
		life.total_life = 20 * 20;
		life.current_life = 20 * 20;
		init_heads();
		init_arc_shooters();
		neck_spin = GetComponentInChildren<SpinForever>();
		core = GetComponentInChildren<aB5Core>();
		phases = new float[]{life.total_life * 0.75f, life.total_life * 0.5f, life.total_life * 0.25f, 0};
		current_phase = 0;
		enter();
	}

	void Update(){
		update_life();
		if(life.current_life <= 0){
			if(heads_showing == 2 ){
				gl.GetComponent<AchievementSystem>().register_achievement(4);
				gl.GetComponent<AchievementSystem>().register_achievement(5);
			} else {
				gl.GetComponent<AchievementSystem>().register_achievement(4);
			}
			ready_to_destroy = true;
		}
		if(deploying_head){
			if(head_deploy_deltatime > head_deploy_time){
				deploying_head = false;
			} else {
				head_deploy_deltatime += Time.deltaTime;
			}
		} else if(entering){
			transform.position = Vector3.MoveTowards(transform.position, boss_deploy.position, Time.deltaTime * boss_deploy_speed);
			if(transform.position == boss_deploy.position){
				entering = false;
				fill_lifebar();
			}
		} else if(filling_lifebar){
			filling_lifebar = life.filling_lifebar;
			if(!filling_lifebar){
				growling = true;
				growl.growl();
				cam_shake.shake(1, growl.growl_duration - 0.5f);
			}
		} else if(growling){
			growling = growl.growling;
			if(!growling){
				fighting = true;
				say_my_name();
			}
		} else if(fighting){
			life.update_lifebar();
			if(current_phase == 0){ // shoot and spin weapons
				if(!weapons_active){
					activate_all_weapons_local();
				}
			} else if(current_phase == 1){
				if(!weapons_active){
					activate_all_weapons_local();
				}
				if(!neck_spinning){
					activate_neck_spin();
				}
				if(!plasma_arc_active){
					projectile_arc_active = false;
					plasma_arc_active = true;
					arc_shooters[1].firing = false;
					arc_shooters[0].firing = true;
				}
			} else if(current_phase == 2){
				if(!weapons_active){
					activate_all_weapons_local();
				}
				if(!neck_spinning){
					activate_neck_spin();
				}
				if(!projectile_arc_active){
					projectile_arc_active = true;
					plasma_arc_active = false;

					arc_shooters[0].firing = false;
					arc_shooters[1].firing = true;
				}
			}
			else if(current_phase == 3){
				if(!weapons_active){
					activate_all_weapons_local();
				}
				if(!neck_spinning){
					activate_neck_spin();
				}
				if(!projectile_arc_active){
					projectile_arc_active = true;
					arc_shooters[1].firing = true;
				}
				if(!plasma_arc_active){
					plasma_arc_active = true;
					arc_shooters[0].firing = true;
				}
			}
			if(life.current_life >= phases[current_phase]){
				// do nothing
			} else {
				current_phase += 1;
				between_phases = true;
				fighting = false;
				phase_transition_deltatime = 0;
				deactivate_all_weapons_local();
				gl.kill_all_bullets();
				cam_shake.shake(0.5f, phase_transition_time - 0.5f);
				Debug.Log(current_phase);
				stop_neck_spin();
			}
		} if(between_phases){
			if(phase_transition_deltatime >= phase_transition_time){
				fighting = true;
				between_phases = false;
				activate_all_weapons_local();
			} else {
				
				phase_transition_deltatime += Time.deltaTime;
			}
		}
	}

	public override void enter ()
	{
		entering = true;
	}

	void fill_lifebar(){
		life.fill_lifebar();
		filling_lifebar = true;
	}

	void update_life(){
		/*
		float tmp_life = 0;
		foreach(aWeapon w in weapons){
			tmp_life += w.life.current_life;
		}
		life.current_life = tmp_life;
		*/
	}

	void deactivate_all_weapons_local(){
		core.set_invencibility(true);
		deactivate_all_weapons();
		set_hydra_weapons_hittable(false);
		foreach(aArcShooter aas in arc_shooters){
			aas.firing = false;
		}
		plasma_arc_active = false;
		projectile_arc_active = false;
	}

	void activate_all_weapons_local(){
		core.set_invencibility(false);
		activate_all_weapons();
		set_hydra_weapons_hittable(true);
		activate_arc_shooters();
	}

	void activate_neck_spin(){
		float speed = 0;
		if(current_phase == 1){
			speed = neck_spin_speed_phase_1;
		}
		if(current_phase == 2){
			speed = neck_spin_speed_phase_2;
		}
		if(current_phase > 2){
			speed = neck_spin_speed_phase_3;
		}
		neck_spin.enabled = true;
		neck_spin.spin_axis_speed = new Vector3(0, speed, 0);
		neck_spinning = true;
	}

	void stop_neck_spin(){
		neck_spin.enabled = false;
		neck_spinning = false;
	}

	public void head_damaged(){
		if(heads_showing > total_heads){
			life.current_life -= 0.1f;
		} else {
			if(!deploying_head){
				show_next_head();
			}
		}
	}

	void show_next_head(){
		deploying_head = true;
		while(heads[heads_showing -1].activeSelf){
			heads_showing += 1;
		}
		heads[heads_showing -1].SetActive(true);
		heads_showing += 1;
		head_deploy_deltatime = 0;
		stop_neck_spin();
		gl.kill_all_bullets();
		deactivate_all_weapons_local();
		if(heads_showing > total_heads){
			turn_off_weapons_colliders();
		}
	}

	void init_heads(){
		heads = new GameObject[10];
		heads_script = new HydraWeapon[20];
		GameObject tmp = GameObject.FindGameObjectWithTag("AB5 Necks Holder");
		for(int i = 0; i < 10; i++){
			heads[i] = tmp.transform.GetChild(i).gameObject;
			HydraWeapon [] heads_tmp = heads[i].GetComponentsInChildren<HydraWeapon>();
			heads_script[i * 2] = heads_tmp[0];
			heads_script[(i * 2) + 1] = heads_tmp[1];
		}

		//heads[0].SetActive(true);
		heads[1].SetActive(false);
		heads[2].SetActive(false);
		heads[3].SetActive(false);
		heads[4].SetActive(false);
		//heads[5].SetActive(true);
		heads[6].SetActive(false);
		heads[7].SetActive(false);
		heads[8].SetActive(false);
		heads[9].SetActive(false);


	}

	void set_hydra_weapons_hittable(bool value){
		foreach(HydraWeapon hw in heads_script){
			hw.hittable = value;
		}
	}

	void init_arc_shooters(){
		arc_shooters = GetComponentsInChildren<aArcShooter>();
	}

	void activate_arc_shooters(){
		if(current_phase == 1){
			plasma_arc_active = true;
			arc_shooters[0].firing = true;
		} else if(current_phase == 2){
			projectile_arc_active = true;
			arc_shooters[1].firing = true;
		}
		else if(current_phase == 3){
			projectile_arc_active = true;
			arc_shooters[1].firing = true;
			plasma_arc_active = true;
			arc_shooters[0].firing = true;
		}
	}
	void turn_off_weapons_colliders(){
		foreach(HydraWeapon hw in heads_script){
			hw.GetComponent<Collider>().enabled = false;
		}
	}
}
