﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class aBossLife : MonoBehaviour {
	public float total_life = 0;
	public float current_life = 0;

	float fill;
	Slider lifebar;

	//states
	public bool filling_lifebar = false;

	void Start(){
		lifebar = GameObject.FindGameObjectWithTag("Boss Lifebar").GetComponent<Slider>();
		fill = 0;
	}

	void FixedUpdate(){
		if(filling_lifebar){
			fill += 1;
			lifebar.value = fill / total_life;
			if(fill >= total_life){
				filling_lifebar = false;
			}
		}
	}

	public void fill_lifebar(){
		if(!filling_lifebar){
			filling_lifebar = true;
			fill = 0;
		}
	}

	public void update_lifebar(){
		lifebar.value = current_life / total_life;
	}

}
