﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TrophyRoomBadge : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public int achievement_index = -1;
	public TrophyRoom room;

	public void OnPointerEnter(PointerEventData eventData){
		room.show(achievement_index);
	}

	public void OnPointerExit(PointerEventData evData){
		room.show_default();
	}
}


