﻿using UnityEngine;
using System.Collections;

public class InputTest : MonoBehaviour {
	void Update(){
		if(Input.GetButton("Next Type")){
			transform.position = new Vector3(
				transform.position.x,
				transform.position.y + 0.1f,
				transform.position.z
			);
		}
		if(Input.GetButton("Prev Type")){
			transform.position = new Vector3(
				transform.position.x,
				transform.position.y - 0.1f,
				transform.position.z
			);
		}

		if(Input.GetButtonDown("Pause")){
			transform.position = new Vector3(
				transform.position.x - 1,
				transform.position.y,
				transform.position.z
			);
		}

		if(Input.GetKeyDown(KeyCode.A)){
			transform.position = new Vector3(
				transform.position.x + 1,
				transform.position.y,
				transform.position.z
			);
		}
	}

}
