﻿using UnityEngine;
using System.Collections;

public class ArcShot : Weapon {
	public int bullet_count = 8;
	public BulletPool bullet_pool;
	public float bullet_speed = 300f;
	public float arc_interval = 0.2f;
	public bool can_shoot = false;

	float arc_deltatime = 0f;
	Vector3 dir;

	void Start(){
		dir = new Vector3(0, 0, 0);
	}

	void Update(){
		if(arc_interval > 0){
			if(arc_deltatime >= arc_interval){
				Shoot();
				arc_deltatime = 0;
			} else {
				arc_deltatime += Time.deltaTime;
			}
		}
	}

	void Shoot(){
		if(can_shoot){
			float bullet_angle = 360f / ((float)bullet_count);
			GameObject newBullet;
			for (int i = 0; i < bullet_count; i++){
				newBullet = bullet_pool.getBullet();
				if(newBullet == null){
					return;
				} else {
					newBullet.transform.position = transform.position;
					newBullet.transform.rotation = Quaternion.Euler(dir);
					newBullet.transform.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bullet_speed);
					dir = new Vector3 (0, dir.y + bullet_angle, 0);
				}
			}
		}
	}
}