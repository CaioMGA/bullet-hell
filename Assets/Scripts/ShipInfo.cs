﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShipInfo : System.Object {
	public int speed;
	public int handling;
	public string dificulty;
	public string full_text;
	public Sprite icon;

	string generate_bar(string sample, int value){
		string bar = "";
		for(int i = 0; i < value; i++){
			bar += sample;
		}
		return bar;
	}

	public string generate_speed_bar(string sample){
		return generate_bar(sample, speed);
	}

	public string generate_handling_bar(string sample){
		return generate_bar(sample, handling);
	}
}
